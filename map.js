var Promise = require("bluebird");
var bhttp = require("bhttp");

Promise.try(function(){
  return bhttp.get("http://somesite.com/all-the-urls.txt");
}).then(function(response){
  return response.body.toString().split("\n");
}).map(function(url){
  return bhttp.get(url);
}).map(function(response){
  return response.body;
}).then(function(arrayOfResponses){
  /* arrayOfResponses now contains all the HTTP responses for each URL in the all-the-urls.txt file! */
}).catch(function(err){
  /* You would normally NOT have a catch-all error handler like this - this is purely for the sake
   * of example. Normally, your error handlers would only catch those errors that it is specfically
   * interested in; an example of this is at https://gist.github.com/joepie91/f6a56acdae303e90e44a */
  console.log("An error occurred!", err);
})